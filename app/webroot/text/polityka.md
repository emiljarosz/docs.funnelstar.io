#POLITYKA PRYWATNOŚCI<br/><br/>
**I. Szczegółowe informacje dotyczące przetwarzania danych użytkowników korzystających ze strony funnelstar.io (klauzula informacyjna)**

1. Administrator Twoich danych osobowych:
TFB Group Spółka z ograniczoną odpowiedzialnością z siedzibą w Krakowie (31-150 Kraków, ul. św. Filipa 23/4) zarejestrowaną w rejestrze przedsiębiorców prowadzonym przez Sąd Rejonowy dla Krakowa – Śródmieścia Wydział XI Krajowego Rejestru Sądowego pod nr KRS: 0000499144, NIP 6762474679, REGON 12304891600000, prowadząca serwis pod adresem: funnelstar.io.

2. Dane kontaktowe Administratora
Z administratorem danych można kontaktować się za pośrednictwem adresu email: info@funnelstar.io, lub pisemnie na następujący adres: 31-150 Kraków, ul. św. Filipa 23/4

3. W jakich celach są przetwarzane Twoje dane osobowe?
Za Twoją dobrowolną zgodą dane mogą być przetwarzane do celów analitycznych (pliki cookies).
Ponadto Twoje dane będą przetwarzane do celu realizacji usług strony funnelstar.io.
Co więcej, Twoje dane mogą być także przetwarzane w celu dopasowania treści witryn internetowych funnelstar.io do Twoich zainteresowań, a także wykrywania botów i nadużyć oraz pomiarów statystycznych i udoskonalenia usług na stronie funnelstar.io.

4. Przez jaki okres będą przechowywane Twoje dane osobowe?
Dane będą przetwarzane w celach analitycznych do momentu wycofania przez Ciebie zgody na takie przetwarzanie.
W odniesieniu do usług strony internetowej funnelstar.io Twoje dane będą przetwarzane przez okres, w którym usługi te będą świadczone oraz, czasami, po zakończeniu ich świadczenia, jednak wyłącznie jeżeli jest dozwolone lub wymagane w świetle obowiązującego prawa np. przetwarzanie w celach statystycznych, rozliczeniowych lub w celu dochodzenia roszczeń. W takim przypadku dane będą przetwarzane jedynie przez okres niezbędny do realizacji tego typu celów.

5. Jaka jest podstawa prawna przetwarzania Twoich danych osobowych?
Podstawą prawną przetwarzania Twoich danych osobowych jest Twoja dobrowolna zgoda na przetwarzanie danych, w tym na cele analityczne (art. 6 ust. 1 lit. a) RODO).
W odniesieniu do usług funnelstar.io, Twoje dane będą przetwarzane na podstawie art. 6 ust. 1 lit b) RODO (jest to niezbędne do wykonania umowy, której stroną jest osoba, której dane dotyczą).
Jeżeli chodzi o wykrywanie botów i nadużyć oraz pomiary statystyczne i udoskonalenie usługi, Twoje dane będą przetwarzane na podstawie art. 6 ust. 1 lit. f) RODO (uzasadniony interes administratora). Uzasadnionym interesem administratora jest dopasowanie treści usług zlokalizowanych na stronach funnelstar.io do Twoich potrzeb, zapewnienie bezpieczeństwa tych usług, a także ich ciągłe udoskonalanie.

6. Wymóg podania danych
Podanie danych w celu realizacji usług witryny funnelstar.io jest niezbędne do świadczenia tych usług. W razie niepodania tych danych usługa nie będzie mogła być świadczona.
Przetwarzanie danych w pozostałych celach wykrywania botów i nadużyć w usługach funnelstar.io, pomiarów statystycznych, analitycznych i udoskonalenia usług funnelstar.io jest niezbędne w celu zapewnienia wysokiej jakości usług funnelstar.io. Niezebranie Twoich danych osobowych w tych celach może uniemożliwić poprawne świadczenie usług.

7. Do jakich odbiorców przekazywane będą Twoje dane osobowe?
Twoje dane mogą być przekazywane podmiotom przetwarzającym dane osobowe na zlecenie administratora m.in. dostawcom usług IT – przy czym takie podmioty przetwarzają dane na podstawie umowy z administratorem i wyłącznie zgodnie z poleceniami administratora.
Twoje dane mogą być również przygotowane podmiotom uprawnionym do ich uzyskania na podstawie obowiązującego prawa np. organom ścigania w razie zgłoszenia przez organ żądania na odpowiedniej podstawie prawnej (np. dla potrzeb toczącego się postępowania karnego).

8. Przekazywanie danych poza EOG
Twoje dane osobowe nie będą przekazywane do odbiorców znajdujących się w państwach poza Europejskim Obszarem Gospodarczym.

9. Jakie są Twoje prawa związane z przetwarzaniem danych osobowych?
przysługują Ci następujące prawa w związku z przetwarzaniem przez Nas Twoich danych osobowych:
– prawo dostępu do Twoich danych, w tym uzyskania kopii danych,
– prawo żądania sprostowania danych
– prawo do usunięcia danych (w określonych sytuacjach),
– prawo wniesienia skargi do organu nadzorczego zajmującego się ochroną danych osobowych,
– prawo do ograniczenia przetwarzania danych.

Jeżeli Twoje dane są przetwarzane na podstawie zgody lub w ramach świadczonej usługi (dane są niezbędne w celu świadczenia usługi) możesz dodatkowo skorzystać z poniższych praw:
– prawo do wycofania zgody w zakresie w jakim są przetwarzane na tej podstawie. Wycofanie zgody nie ma wpływu na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej wycofaniem.
– prawo do przenoszenia danych osobowych, tj. do otrzymania od administratora Twoich danych osobowych, w ustrukturyzowanym, powszechnie używanym formacie nadającym się do odczytu maszynowego. Możesz przesłać te dane innemu administratorowi danych.

Jeżeli Twoje dane przetwarzane są w oparciu o uzasadniony interes administratora danych, przysługuje Ci prawo wniesienia sprzeciwu wobec takiego przetwarzania.

W celu skorzystania z powyższych praw należy skontaktować się z administratorem danych. Dane kontaktowe wskazane są wyżej.

**II. Ogólne informacje dotyczące przetwarzania danych Użytkowników przez TFB Group sp. z o.o.**

1. Administratorem danych jest TFB Group Spółka z ograniczoną odpowiedzialnością z siedzibą w Krakowie (31-150 Kraków, ul. św. Filipa 23/4) zarejestrowaną w rejestrze przedsiębiorców prowadzonym przez Sąd Rejonowy dla Krakowa – Śródmieścia Wydział XI Krajowego Rejestru Sądowego pod nr KRS: 0000499144, NIP 6762474679, REGON 12304891600000 (Administrator).

2. Z Administratorem można się kontaktować we wszystkich sprawach związanych z przetwarzaniem danych osobowych pod następującym adresem e-mail: info@funnelstar.io lub korespondencyjnie na adres podany powyżej.

3. Dane osobowe mogą być zbierane przez Administratora w następujących celach:
a. świadczenia usług/ wykonania umowy przez Administratora np.: zgodnie z Regulaminem świadczenia usług lub strony funnelstar.io – art. 6 ust. 1 lit. b) RODO;
b. przesyłania zamówionych informacji handlowych, w tym newslettera na podstawie zgody – art. 6 ust. 1 lit. a) RODO;
c. zapewnienia kontaktu z Administratorem za pośrednictwem formularza kontaktowego – art. 6 ust. 1 lit. f) RODO;
d. księgowo-podatkowych– art. 6 ust. 1 lit. c) RODO;
e. dochodzenia roszczeń w zw. z zawartymi umowami z Użytkownikami – art. 6 ust. 1 lit. f) RODO;
f. wykrywania botów i nadużyć w usługach, pomiarów statystycznych, analitycznych i udoskonalenia usług – art. 6 ust. 1 lit. f) RODO.

4. Administrator zbiera dane osobowe wyłącznie w zakresie niezbędnym dla realizacji celów, do których są zbierane.

5. Administrator przetwarza dane osobowe wyłącznie przez czas niezbędny dla realizacji zamierzonych celów.

6. Użytkownikom przysługują następujące prawa, z których mogą skorzystać (w tym celu należy się skontaktować z Administratorem – dane kontaktowe powyżej):
a. Prawo dostępu do danych;
b. Prawo do sprostowania danych;
c. Prawo do usunięcia danych;
d. Prawo do ograniczenia przetwarzania;
e. Prawo do przenoszenia danych (o ile do przetwarzania dochodzi na podstawie zgody lub w celu zawarcia umowy);
f. Prawo sprzeciwu (o ile dane przetwarzane są na podstawie uzasadnionego interesu administratora);
g. Prawo do cofnięcia zgody, o ile przetwarzanie następuje na podstawie zgody;
h. Prawo wniesienia skargi do organu nadzorczego zajmującego się ochroną danych osobowych.

7. Dane mogą być przekazywane podmiotom przetwarzającym dane osobowe na zlecenie Administratora m.in. dostawcom usług IT – przy czym takie podmioty przetwarzają dane na podstawie umowy z Administratorem i wyłącznie zgodnie z poleceniami Administratora. Dane mogą być również przekazane podmiotom uprawnionym do ich uzyskania na podstawie obowiązującego prawa np. organom ścigania w razie zgłoszenia przez organ żądania na odpowiedniej podstawie prawnej (np. dla potrzeb toczącego się postępowania karnego).

8. Dane osobowe nie będą przekazywane do odbiorców znajdujących się w państwach poza Europejskim Obszarem Gospodarczym.

9. Administrator dokłada wszelkich starań, aby zapewnić wszelkie środki fizycznej, technicznej i organizacyjnej ochrony danych osobowych przed ich przypadkowym czy umyślnym zniszczeniem, przypadkową utratą, zmianą, nieuprawnionym ujawnieniem, wykorzystaniem czy dostępem, zgodnie ze wszystkimi obowiązującymi przepisami.

10. Pełne informacje o przetwarzaniu danych osobowych znajdują się w klauzulach informacyjnych przekazywanych Użytkownikom w momencie zbierania od nich danych osobowych dla określonych celów.

**III. Postanowienia końcowe**

1. Korzystając ze strony internetowej funnelstar.io jak i z funkcji Serwisu, wyrażasz zgodę na postanowienia zawarte w niniejszej Polityce Prywatności, która jest nieodłącznym elementem Regulaminu.

2. Usługodawca zastrzega sobie prawo do wprowadzania zmian w niniejszej Polityce Prywatności, z tym zastrzeżeniem, że zmiany te nie wpłyną na to, że funnelstar.io nigdy nie udostępni danych Użytkowników osobom trzecim.
