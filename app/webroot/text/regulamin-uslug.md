#REGULAMIN SERWISU FUNNELSTAR.io<br/><br/>

**I. Postanowienia ogólne**

1. Regulamin niniejszy określa rodzaje, zakres i warunki świadczenia usług drogą elektroniczną przez TFB Group Spółka z ograniczoną odpowiedzialnością z siedzibą w Krakowie (31-150 Kraków, ul. św. Filipa 23/4) zarejestrowaną w rejestrze przedsiębiorców prowadzonym przez Sąd Rejonowy dla Krakowa – Śródmieścia Wydział XI Krajowego Rejestru Sądowego pod nr KRS: 0000499144, NIP 6762474679, REGON 12304891600000 za pośrednictwem Serwisu zlokalizowanego pod adresem: https://market.funnelstar.io/.
2. Każdy, kto zamierza skorzystać z Serwisu oraz oferowanych usług zobowiązany jest zapoznać się z jego Regulaminem.
3. Korzystając z Serwisu, Użytkownik oświadcza i potwierdza, że zapoznał się z treścią niniejszego Regulaminu, akceptuje go i zobowiązuje się do przestrzegania jego postanowień.
4. W sytuacji, gdy zasady korzystania z usług oferowanych w ramach Serwisu regulowane są szczególnymi regulacjami (odrębne regulaminy dla poszczególnych usług), Użytkownik zobowiązany jest zapoznać się i stosować również te postanowienia szczegółowe.
5. O ile nie zastrzeżono inaczej w odrębnych regulaminach, nin. Regulaminowi podlegają wszystkie usługi świadczone za pośrednictwem Serwisu.

**II. Definicje**

>Dla uniknięcia wątpliwości, użyte w Regulaminie zwroty oznaczają:
Serwis – strona internetowa znajdująca się pod adresem https://market.funnelstar.io/ lub innymi adresami w domenie funellstar;
Usługodawca – TFB Group Spółka z ograniczoną odpowiedzialnością z siedzibą w Krakowie (31-150 Kraków, ul. św. Filipa 23/4) zarejestrowaną w rejestrze przedsiębiorców prowadzonym przez Sąd Rejonowy dla Krakowa – Śródmieścia Wydział XI Krajowego Rejestru Sądowego pod nr KRS: 0000499144;
Użytkownik – każda osoba, która w jakikolwiek sposób korzysta z Usług oferowanych w Serwisie;
Usługa – usługi świadczone drogą elektroniczną przez Usługodawcę na rzecz Użytkowników dostępne w Serwisie a szczegółowo opisane w rozdziale III pkt 1 lub w innych regulaminach odnoszących się do konkretnych usług;
Materiały – teksty, zdjęcia, grafiki, materiały multimedialne i inne, które stanowią utwór w rozumieniu ustawy o prawie autorskim i prawach pokrewnych;
Regulamin – nin. Regulamin wraz ze wszystkimi załącznikami.

**III. Rodzaje i zakres usług**

1. W Serwisie dostępne są m.in. następujące Usługi:
a. Dostępu do Materiałów publikowanych w Serwisie.
b. Newsletter – usługa polegająca na przesyłaniu na podany adres poczty elektronicznej Użytkownikom, którzy wyrazili taką wolę (zgodę) za pomocą narzędzi dostępnych w Serwisie, informacji handlowych o produktach i usługach Usługodawcy; zgoda na otrzymywanie Newslettera w każdym czasie może być odwołana.
c. Formularz kontaktowy – usługa polegająca na udostępnieniu w Serwisie narzędzi umożliwiających kontakt z Usługodawcą.
d. Inne Usługi szczegółowo opisane w odrębnych regulaminach.
2. Świadczenie Usług oferowanych w Serwisie na rzecz Użytkowników odbywa się na warunkach określonych w Regulaminie, z uwzględnieniem szczególnych zasad odnoszących się do Usług opisanych w odrębnych regulaminach.
3. Do zawarcia pomiędzy Usługodawcą, a Użytkownikiem umowy o świadczenie Usług w Serwisie dochodzi w momencie wpisania przez Użytkownika adresu strony www Serwisu w przeglądarce internetowej, a do rozwiązania umowy o świadczenie Usług dochodzi z chwilą zamknięcia przez Użytkownika strony www Serwisu.
4. Zasady zawierania oraz rozwiązywania umów o świadczenie Usług określonych w odrębnych regulaminach określają poszczególne regulaminy dotyczące tych Usług.
5. Regulamin stanowi integralną cześć zawieranych przez Usługodawcę z Użytkownikiem umów o świadczenie Usług szczegółowo opisanych na łamach odrębnych regulaminów, stosuje się go w zakresie nieuregulowanym w odrębnych regulaminach.

**IV. Prawa i obowiązki Użytkownika i Usługodawcy**

1. Użytkownik zobowiązuje się do przestrzegania niniejszego Regulaminu.
2. Użytkownik zobowiązany jest do powstrzymania się od działań:
a. powodujących zachwianie pracy lub przeciążenie systemów teleinformatycznych Usługodawcy,
b. naruszających powszechnie obowiązujące przepisy prawa, w tym prawa autorskie, dobra osobiste, zasady współżycia społecznego, godzących w dobre obyczaje,
3. Użytkownik ponosi odpowiedzialność za wszelkie następstwa nieprzestrzegania zasad określonych w pkt 1-3 powyżej.
4. Usługodawca, w przypadkach szczególnych, ma prawo do czasowego zaprzestania lub ograniczenia świadczenia Usług, bez wcześniejszego powiadomienia oraz do przeprowadzenia niezbędnych prac służących przywróceniu bezpieczeństwa i stabilności systemu teleinformatycznego.
5. Usługodawca nie gwarantuje niezakłóconego i nieprzerwanego dostępu do Usług z uwagi na właściwości sieci Internet oraz sprzętu komputerowego Użytkownika.
6. Usługodawca nie ponosi odpowiedzialności za przerwy w świadczeniu Usług lub ich niewłaściwe funkcjonowanie, obniżenie jakości spowodowane parametrami sprzętu, oprogramowania czy sieci, z których korzysta Użytkownik.
7. Usługodawca zastrzega sobie prawo do przerwania świadczenia Usług określonym Użytkownikom w przypadku stwierdzenia naruszenia przez nich Regulaminu lub dopuszczenia się działań określonych w pkt 2-3 powyżej.
8. Usługodawca nie ponosi odpowiedzialności za szkody powstałe na skutek udostępnienia przez Użytkownika osobom trzecim danych autoryzujących ich dostęp do Usług lub nieodpowiedniego zabezpieczenia tych danych, a także innych haseł, które Użytkownik winien zachować w poufności.

**V. Własność intelektualna**
1. Wszelkie publikowane w Serwisie Materiały, znaki towarowe, bazy danych, w tym także te składające się na treść Usług, podlegają ochronie na zasadach określonych we właściwych przepisach, w tym m.in. w: ustawie o prawach autorskich i prawach pokrewnych, ustawie o bazach danych, ustawie o zwalczaniu nieuczciwej konkurencji oraz prawie własności przemysłowej.
2. Użytkownikowi przysługuje prawo do korzystania z treści opisanych powyżej wyłącznie w zakresie użytku osobistego. Wykorzystanie treści w sposób wykraczający poza osobisty użytek wymaga zgody Usługodawcy.

**VI. Warunki techniczne oraz zagrożenia**
1. Celem skorzystania z Usługi, Użytkownik winien spełnić następujące wymagania techniczne, niezbędne do współpracy z systemem teleinformatycznym Usługodawcy:
a) dostęp do sieci Internet;
b) posiadanie konta poczty elektronicznej (e-mail),
c) zainstalowana przeglądarka internetowa: Opera, Firefox, Chrome, Safari. Korzystając z innych przeglądarek internetowych Usługodawca nie gwarantuje poprawnego działania Serwisu i dostępnych za jego pośrednictwem Usług.
2. Odrębne regulaminy dotyczące poszczególnych usług mogą określać dodatkowe warunki techniczne niezbędne do korzystania z tych usług.
3. Usługodawca zastrzega, że ze względu na publiczny charakter sieci Internet, która używana jest m.in. do połączenia z Serwisem, świadczenie Usług może wiązać się z ryzykiem, które Użytkownik akceptuje, godząc się na korzystanie z publicznych kanałów teleinformatycznych. W związku ze szczególnym charakterem zagrożeń elektronicznych, Usługodawca nie gwarantuje pełnego bezpieczeństwa użytkowania Serwisu. Dlatego też, w celu minimalizacji ryzyka i podniesienia poziomu bezpieczeństwa, Usługodawca zaleca, aby Użytkownik we własnym zakresie zabezpieczył swoje urządzenie końcowe, w szczególności poprzez wprowadzenie mechanizmów kontroli dostępu, ochronę danych służących do uwierzytelniania dostępu, instalację oprogramowania antywirusowego, posiadanie aktualnego systemu operacyjnego, a także podłączanie urządzeń końcowych wyłącznie do wiarygodnych sieci Wi-Fi.

**VII. Dane osobowe i pliki cookies**
1. Administratorem danych osobowych Użytkowników jest Usługodawca.
2. Usługodawca oświadcza, że chroni dane osobowe Użytkowników na zasadach i zgodnie z obowiązującymi przepisami z zakresu ochrony danych osobowych, w szczególności Rozporządzeniem Parlamentu Europejskiego i Rady 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych/RODO).
3. Usługodawca oświadcza, że stosuje środki techniczne i organizacyjne zapewniające ochronę przetwarzanych danych odpowiednią do zagrożeń oraz kategorii danych objętych ochroną, a w szczególności zabezpiecza dane osobowe Użytkowników przed udostępnieniem ich osobom nieupoważnionym, utratą czy uszkodzeniem.
4. Szczegółowe informacje dla Użytkowników dot. przetwarzania danych osobowych, w tym celów, podstaw przetwarzania ze wskazaniem przepisów prawa, okresów przetwarzania oraz przysługujących im praw zawarte są w Polityce prywatności dostępnej tutaj: LINK.0
5. Usługodawca informuje, iż podczas korzystania z Serwisu na urządzeniach Użytkownika zapisywane są krótkie informacje tekstowe zwane cookies. Instalacja cookies jest niezbędna do prawidłowego świadczenia Usług w Serwisie. Cookies nie pozwalają na identyfikację Użytkownika. Za pomocą cookies nie są przetwarzane lub przechowywane dane osobowe. Szczegółowe informacje na temat plików cookies znajdują się w Polityce prywatności dostępnej tutaj: LINK
6. Polityka prywatności stanowi integralną część nin. Regulaminu

**VIII. Postępowanie reklamacyjne**
1. Użytkownik ma prawo składać reklamacje w zw. z Usługami świadczonymi w Serwisie oraz w zw. z funkcjonowaniem Serwisu.
2. Reklamacje winny być składane w formie e-mail na adres: info@funnelstar.io
3. Reklamację należy składać nie później niż w ciągu 7 (siedmiu) dni od dnia, kiedy wystąpiła przyczyna reklamacji.
4. Każda reklamacja winna zawierać krótki opis problemu będącego podstawą jej złożenia, datę i godzinę jego wystąpienia oraz dane identyfikujące Użytkownika umożliwiające przedstawienie mu odpowiedzi na reklamację.
5. Usługodawca rozpatruje reklamacje w terminie 30 dni od dnia jej otrzymania. Odpowiedź na reklamację przekazywana jest za pośrednictwem adres e-mail podany w zgłoszeniu reklamacji.
6. Usługodawca zastrzega sobie prawo do wydłużenia terminu określonego w pkt 5 powyżej, w sytuacji, gdy rozpatrzenie reklamacji wymaga podjęcia dodatkowych działań, wyjaśnień oraz uzyskania dodatkowych informacji. W takich sytuacjach Usługodawca w tym terminie poinformuje Użytkownika o wydłużonym terminie rozpoznania reklamacji, z zastrzeżeniem że maksymalny czas rozpoznania reklamacji nie może przekroczyć 60 dni.
7. Usługodawca zastrzega, że reklamacje nie zawierające informacji określonych w pkt 4 powyżej nie będą rozpatrywane.

**IX. Postanowienia końcowe**
1. Niniejszy Regulamin obowiązuje od dnia opublikowania go w Serwisie.
2. Usługodawca zastrzega sobie możliwość zmiany Regulaminu, w zakresie dopuszczanym przez obowiązujące przepisy, w szczególności modyfikacje mogą być wynikiem zmian obowiązujących przepisów.
3. Usługodawca za każdym razem poinformuje Użytkowników o zmianie Regulaminu umieszczając odpowiedni komunikat w Serwisie. Zmiany Regulaminu wchodzą w życie z chwilą ich publikacji w Serwisie. Jeżeli Użytkownik nie akceptuje wprowadzonych zmian powinien powstrzymać się od dalszego korzystania z Serwisu.
4. Regulamin jest na stałe dostępny nieodpłatnie w Serwisie, na stronie www.funnelstar.io/regulamin w formie, która umożliwia jego pozyskanie, odtwarzanie i utrwalanie za pomocą systemu teleinformatycznego, którym posługuje się Użytkownik.
5. We wszystkich sprawach związanych z funkcjonowaniem Serwisu i Usługami prosimy o kontakt za pośrednictwem e-mail na adres: info@funnelstar.io
6. W sprawach nieuregulowanych w niniejszym Regulaminie lub regulaminach poszczególnych usług mają zastosowanie obowiązujące w tym zakresie przepisy prawa polskiego.
7. Wszelkie spory związane z korzystaniem z Serwisu oraz Usług dostępnych za jego pośrednictwem, rozstrzygane będą przez właściwe sądy polskie.
8. W przypadku Użytkownika, który jest konsumentem istnieje możliwość skorzystania z pozasądowego sposobu rozstrzygania sporów. Szczegółowe informacje w tym przedmiocie dostępne są w siedzibach oraz na stronach internetowych powiatowych (miejskich) rzeczników konsumentów, organizacji społecznych, do których zadań statutowych należy ochrona konsumentów (Federacja Konsumentów, Stowarzyszenie Konsumentów Polskich), Wojewódzkich Inspektoratów Inspekcji Handlowej.
