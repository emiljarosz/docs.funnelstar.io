#ZAŁĄCZNIK NR  1 WZÓR FORMULARZA ODSTĄPIENIA OD UMOWY</br></br>
(formularz ten należy wypełnić i odesłać tylko w przypadku chęci odstąpienia od umowy)

Adresat [w tym miejscu przedsiębiorca powinien wpisać nazwę przedsiębiorcy, pełny adres pocztowy oraz, o ile są dostępne, numer faksu i adres e-mail]</br>
…………………………………………………………………………………………………..</br>
…………………………………………………………………………………………………..</br>
…………………………………………………………………………………………………..</br>

Ja/My(*) niniejszym informuję/informujemy(*) o moim/naszym odstąpieniu od umowy sprzedaży następujących rzeczy(*)
umowy dostawy następujących rzeczy(*) umowy o dzieło polegającej na wykonaniu następujących rzeczy(*)/o świadczenie
następującej usługi(*)</br>
…………………………………………………………………………………………………..</br>
…………………………………………………………………………………………………..</br>

Data zawarcia umowy(*)/odbioru(*)</br>
…………………………………………………………………………………………………..</br>

Imię i nazwisko konsumenta(-ów)</br>
…………………………………………………………………………………………………..</br>

Adres konsumenta(-ów)</br>
…………………………………………………………………………………………………..</br>
…………………………………………………………………………………………………..</br>

Podpis konsumenta(-ów) (tylko jeżeli formularz jest przesyłany w wersji papierowej)</br>
…………………………………………………………………………………………………..</br>
…………………………………………………………………………………………………..</br>

Data</br>
…………………………………………………………………………………………………..</br>

(*) Niepotrzebne skreślić.
