#REGULAMIN ŚWIADCZENIA USŁUGI BLOGA<br/><br/>
**I. Postanowienia ogólne**

1. Niniejszy Regulamin określa zakres i zasady świadczenia drogą elektroniczną Usługi Bloga (Usługa), przez TFB GROUP SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą ul. św. Filipa 23/4, 31-150 Kraków zarejestrowana w rejestrze przedsiębiorców Krajowego Rejestru Sądowego prowadzonego przez Sąd Rejonowy dla Krakowa- Śródmieścia w Krakowie, XI Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS: 0000499144, NIP: 6762474679, REGON: 123048916 za pośrednictwem Serwisu zlokalizowanego pod adresem: https://market.funnelstar.io/.
2. Świadczenie Usługi odbywa się za pomocą Serwisu na zasadach określonych w niniejszym Regulaminie oraz Regulaminie Serwisu Funnelstar.
3. W przypadku rozbieżności w postanowieniach nin. Regulaminu oraz Regulaminu Serwisu Funnelstar, pierwszeństwo mają postanowienia nin. Regulaminu.
4. O ile Regulamin nie stanowi inaczej, wszelkie oświadczenia woli, zawiadomienia i kontakty pomiędzy Usługodawcą a Usługobiorcą następują w formie elektronicznej, poprzez adres email Usługodawcy: info@funnelstar.io. W przypadku zmiany adresu email lub sposobu kontaktu wskazanego w Regulaminie Usługodawca poinformuje o tym Usługobiorcę e-mailowo na adres podany przez Usługobiorcę przy procesie zamówienia Usługi.
5. Usługobiorca poprzez złożenie zamówienia na Usługę opisaną w niniejszym Regulaminie w sposób przewidziany w Regulaminie, potwierdza jednocześnie, że zapoznał się z Regulaminem i Regulaminu Serwisu Funnelstar oraz, że akceptuje jego postanowienia. Złożenie zamówienia i zapłata za Usługę stanowi zawarcie umowy o świadczenie usług drogą elektroniczną w rozumieniu ustawy z dnia 18 lipca 2002 r. o  świadczeniu usług drogą elektroniczną (Dz.U. 2002 nr 144 poz. 1204), zwana dalej u.ś.u.d.e.

**§1 Definicje**
1. Blog – przypisana Usługobiorcy strona internetowa zawierająca odrębne, uporządkowane chronologicznie wpisy, której motywem przewodnim jest określony temat.
2. Panel Usługobiorcy – indywidualnie przypisane dla danego Usługobiorcy konto umożliwiające mu czynności administracyjne związane z Usługą, a to m.in.: zmianę hasła, pobranie faktury, zapoznanie się z ofertą Usługodawcy.
3. Panel Bloga – przypisany do danego Bloga zespół narzędzi informatycznych umożliwiających zarządzanie Blogiem, w tym dodawanie, usuwanie, zmianę i pobieranie treści z niego.
4. Usługa/Usługa Pakiet Blog – usługi świadczone drogą elektroniczną przez Usługodawcę przy pomocy portalu Funnelstar opisane w §2.
5. Konsument- osoba fizyczna dokonującą z przedsiębiorcą czynności prawnej niezwiązanej bezpośrednio z jej działalnością gospodarczą lub zawodową.
6. Materiały – teksty, zdjęcia, grafiki, materiały multimedialne i inne, które stanowią utwór w rozumieniu ustawy o prawie autorskim i prawach pokrewnych, do których Usługodawca uzyskał tytuł prawny umożliwiający mu dysponowanie nimi.
7. Okres testowy – okres 7 dni, w ciągu którego Usługobiorca ma możliwość zapoznania się z Usługą, za obniżoną ceną. Każdy Usługobiorca może tylko raz skorzystać z Okresu testowego przewidzianego dla Usługi.
8. Serwis – strona internetowa znajdująca się pod adresem https://market.funnelstar.io/ lub innymi adresami w domenie funellstar.
9. Regulamin- nin. regulamin opisujący warunki i zasady świadczenia drogą elektroniczną Usługi Bloga.
10. Regulamin Serwisu Funnelstar/ Regulamin ogólny- regulamin określający rodzaje, zakres i  warunki świadczenia usług drogą elektroniczną przez Usługodawcę przy pomocy Serwisu.
11. RODO – Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z  dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z  przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE.
12. Umowa/ Umowa o świadczenie Usług – umowa zawarta z Usługobiorcą w ramach zorganizowanego systemu zawierania umów na odległość, bez jednoczesnej fizycznej obecności stron, z wyłącznym wykorzystaniem jednego lub większej liczby środków porozumiewania się na odległość.
13. Usługobiorca- osoba fizyczna, osoba prawna albo jednostka organizacyjna nieposiadająca osobowości prawnej, która korzysta z usług świadczonych drogą elektroniczną przez Usługodawcę.
14. Usługodawca- TFB GROUP SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą ul. św. Filipa 23/4, 31-150 Kraków zarejestrowana w rejestrze przedsiębiorców Krajowego Rejestru Sądowego prowadzonego przez Sąd Rejonowy dla Krakowa- Śródmieścia w Krakowie, XI Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS: 0000499144, NIP: 6762474679, REGON: 123048916.
15. Użytkownik- każda osoba, która w jakikolwiek sposób korzysta z usług oferowanych w Serwisie.

**§2 Rodzaje i zakres usług objętych Regulaminem**
1. Usługodawca, w oparciu o nin. Regulamin świadczy Usługobiorcom Usługę płatną prowadzenia Bloga Pakiet Blog w dwóch wersjach: Pakiet Blog VIP i Pakiet Blog Pro, zwane dalej łącznie Usługą, opisane poniżej:
2. Pakiet Blog PRO obejmuje następujące czynności:
a) zapewnienie domeny,
b) zapewnienie miejsca na serwerze,
c) stworzenie Bloga od strony informatycznej oraz graficznej,
d) tworzenie i umieszczanie na Blogu 3 artykułów miesięcznie,
e) zamieszczenie grafik lub zdjęć przy wpisach na Blogu.
3. Pakiet Blog VIP obejmuje następujące czynności:
a) zapewnienie domeny,
b) zapewnienie miejsca na serwerze,
c) stworzenie Bloga od strony informatycznej oraz graficznej,
d) tworzenie i umieszczanie na Blogu 5 artykułów miesięcznie,
e) utworzenie ankiety dla użytkowników Bloga i załączenie jej do jednej z wiadomości e-mail wysyłanych do nich,
f) przekazywanie Usługobiorcy comiesięcznych sugestii nt. produktów i treści, jakie winny pojawić się na Blogu w oparciu o wyniki ankiet.

II. Postanowienia szczególne

**§3 Wymagania techniczne**
1. Celem skorzystania z Usługi, Usługobiorca, oprócz wymagań określonych w Regulaminie ogólnym, winien fakultatywnie posiadać nr telefonu komórkowego, na który (obok na podany adres e-mail) otrzyma login oraz hasło do indywidualnego Panelu Usługobiorcy w Serwisie.

**§4 Proces zamówienia Usługi**
2. Użytkownik chcąc zamówić Usługę winien:
a) wybrać w Serwisie typ Usługi: Pakiet Blog PRO/ Pakiet Blog VIP, a następnie opcję „kup teraz”;
b) uzupełnić swoje dane osobowe w ramach formularza;
c) zaakceptować Regulamin;
d) nacisnąć ikonę ,,zamów i zapłać”;
e) dokonać opłaty za Okres testowy danej Usługi;.
3. Do zawarcia Umowy o świadczenie Usługi dochodzi w chwili spełnienia przez Usługobiorcę wszystkich czynności opisanych w ust.1.
4. Po dokonaniu czynności wskazanych w ust.1 Usługodawca udostępnia Usługobiorcy login i hasło do Panelu Usługobiorcy, przesyłając je na podany przez Usługobiorcę adres email i numer telefonu (o ile został podany podczas zamówienia Usługi).
5. Panel Usługobiorcy zostaje automatycznie aktywowany po dokonaniu czynności opisanych w ust.1 przez Usługobiorcę lub w przypadku awarii systemu lub problemów technicznych, po dokonaniu aktywacji ręcznej przez Usługodawcę najdalej w ciągu 3dni roboczych.
6. Pierwszy dostęp do Panelu Usługobiorcy jest zapewniony przy użyciu loginu i hasła otrzymanego w sposób określony w ust. 3 powyżej. W dalszej kolejności Usługobiorca może zmieniać te dane przy użyciu funkcjonalności w Panelu Usługobiorcy.

**§5 Proces świadczenia Usługi Pakiet Blog**
1. Usługobiorca po zamówieniu Usługi, zgodnie z par. 4 powyżej, otrzymuje na podany przez siebie adres e-mail link do materiałów opisujących proces powstania Bloga. Usługobiorca winien zapoznać się z otrzymanymi materiałami oraz wypełnić i wysłać udostępnioną przez Usługodawcę ankietę, co jest warunkiem koniecznym rozpoczęcia prac nad Blogiem.
2. W przypadku braku wypełnienia i odesłania ankiety, o której mowa w ust. 1 powyżej, Usługodawca przypomni jednorazowo o konieczności jej wypełnienia wysyłając w tym celu wiadomość e-mail.
3. Najdalej w terminie do 30 dni od dnia dokonania pierwszej miesięcznej opłaty za Usługę, o czym mowa w par. 11 ust. 1 oraz otrzymania przez Usługodawcę wypełnionej ankiety, Usługodawca uruchamia Bloga z pierwszymi artykułami. Wraz z uruchomieniem Bloga, Usługodawca przekazuje Usługobiorcy za pośrednictwem e-mail, login i hasło do Panelu Bloga.
4. W przypadku gdy Usługobiorca posiada już własnego Bloga, którego dalsze prowadzenie oraz rozbudowę zgodnie z par. 2 powyżej zleca Usługodawcy, Usługobiorca niezwłocznie przekazuje Usługodawcy dane niezbędne do prowadzenia Bloga w postaci: loginu i hasła do panelu tego Bloga, po czym Usługodawca podejmuje czynności techniczne prowadzące do zamieszczenia Bloga we własnej infrastrukturze informatycznej celem rozpoczęcia świadczenia Usługi. Co do dalszych czynności zastosowanie mają odpowiednio zapisy ust. 1-3 powyżej.

**§6 Prawa i obowiązki Usługobiorcy**
1. Usługobiorca podczas procesu zamawiania Usługi podaje swoje prawdziwe oraz aktualne dane.
2. W przypadku zmiany danych w trakcie trwania umowy Usługobiorca niezwłocznie informuje o tym fakcie Usługodawcę w sposób opisany w §14.
3. Usługobiorca powierza prowadzenie Bloga Usługodawcy i akceptuje dobór przez Usługodawcę Materiałów do Bloga z uwzględnieniem preferencji i uwag przekazanych przez Usługobiorcę w ramach ankiety, o której mowa w par. 5 ust. 1.
4. Usługobiorca może, niezależnie od Usługodawcy, umieszczać na Blogu swoje artykuły i publikacje oraz zarządzać Blogiem wykorzystując w tym celu Panel Bloga.
5. Usługobiorca może w sposób określony w par. 8 ust. 4 korzystać z Materiałów przygotowanych przez Usługodawcę i umieszczonych na Blogu.
6. Usługobiorca zobowiązuje się do nie ingerowania w kod źródłowy Bloga.
7. Usługobiorca zobowiązuje się do współpracy z Usługodawcą celem realizacji Usługi, w szczególności do wypełnienia ankiety o której mowa w par. 5 ust. 1 oraz udzielania, w razie potrzeby, dalszych informacji.
8. Bez uprzedniej zgody Usługodawcy, Usługobiorca nie może przenieść na osobę trzecią praw i obowiązków wynikających z zawartej Umowy o świadczenie Usług.

**§7 Prawa i obowiązki Usługodawcy**
1. Usługodawca zobowiązuje się do świadczenia Usługi w sposób należyty wynikający z profesjonalnego charakteru świadczenia usług tego rodzaju.
2. Usługodawca zobowiązuje się do przygotowania Bloga oraz Materiałów na niego z uwzględnieniem danych oraz informacji przekazanych przez Usługobiorcę.
3. Usługodawca nie będzie publikował na Blogu Materiałów, co do których istnieje uzasadniona wątpliwość zgodności z przepisami prawa, dobrymi obyczajami, normami społecznymi i obyczajowymi.
4. Usługodawca ma prawo wprowadzić bezterminową blokadę Panelu Usługobiorcy w przypadku gdy:
a) Usługodawca ustali, że podczas procesu zamawiania Usługi opisanym w §4 Usługobiorca podał nieprawdziwe dane,
b) Usługobiorca zalega z zapłatą za świadczenie Usługi za co najmniej jeden okres rozliczeniowy.

**§8 Własność intelektualna**
1. Usługodawca oświadcza, że na mocy umów o przeniesienie praw lub licencyjnych przysługują mu odpowiednie uprawnienia do Materiałów pozwalające na zamieszczanie ich na Blogu i eksploatację w sposób opisany poniżej.
2. Jednocześnie Usługodawca oświadcza, że publikowane na Blogu Materiały są wolne od wszelkich wad fizycznych i prawnych oraz roszczeń osób trzecich.
3. W ramach świadczenia Usługi, Usługodawca udziela Usługobiorcy, bez odrębnego wynagrodzenia, licencji do korzystania z Materiałów umieszczonych przez Usługodawcę na Blogu.
4. Zakres licencji określonej w ust. 3 powyżej obejmuje:
a. Prawo do umieszczania Materiałów na Blogu w taki sposób, aby każdy mógł mieć do nich dostęp w czasie i miejscu dowolnie wybranym;
b. Prawo do publikacji Materiałów w mediach społecznościowych, w szczególności na prowadzonych przez Usługobiorcę profilach, stronach internetowych;
c. Prawo do prezentowania Materiałów w postaci drukowanej w ramach prowadzonej działalności marketingowo-reklamowej związanej z Blogiem;
d. Prawo do modyfikowania, skracania, Materiałów.
5. Licencja, o której mowa w ust. 3 powyżej udzielona jest na czas świadczenia Usługi, z zastrzeżeniem ust. 6 poniżej i jest nieograniczona terytorialnie.
6. Po zakończeniu świadczenia Usługi, Usługodawca, o ile taką wolę wyrazi Usługobiorca, może przenieść na niego prawa autorskie lub udzielić mu licencji do Materiałów zamieszczonych na Blogu za odrębnym wynagrodzeniem i na zasadach każdorazowo ustalanych przez strony.

**§9 Odpowiedzialność**
1. Usługodawca nie ponosi odpowiedzialności za zamieszczone przez Usługobiorcę, zgodnie z par. 6 ust. 4, na Blogu Materiały.
2. Usługodawca nie odpowiada za skutki oraz następstwa świadczonej Usługi, w szczególności braku pozyskania zakładanej liczby użytkowników Bloga, decyzje oraz działania podjęte w oparciu o publikowane na Blogu Materiały.
3. Usługodawca nie ponosi odpowiedzialności w przypadku przerw lub zakłóceń w dostępności i korzystaniu z Usług, jeżeli jest to spowodowane:
a) koniecznością naprawy, rozbudowy, modyfikacji lub konserwacji sprzętu lub oprogramowania;
b) przyczynami niezależnymi od Usługodawcy (siła wyższa, działania lub zaniechania osób trzecich).
4. Usługodawca nie odpowiada za błędy w prawidłowym funkcjonowaniu Bloga będące następstwem dokonanych przez Usługobiorcę zmian w kodzie źródłowym, jak również podejmowanymi samodzielnie innymi czynnościami (dodawaniem/usuwaniem Materiałów).

**§10 Ceny Usług**
1. Ceny Usług podane są w Serwisie przy opisie danej Usługi.
2. Wszystkie podane w Serwisie ceny Usług są cenami brutto i zawierają podatek VAT w wysokości wynikającej z obowiązujących przepisów prawa.
3. Wiążąca dla Usługodawcy i Usługobiorcy jest cena podana w Serwisie przy Usłudze w chwili zamówienia Usługi.
4. Usługodawca, zastrzega sobie prawo do zmiany cen Usług, przeprowadzania akcji promocyjnych oraz udzielania rabatów. Odbywa się to z zachowaniem praw nabytych przez Usługobiorców oraz w granicach dopuszczalnych przez obowiązujące przepisy prawa. Zmiany cen nie wpływają na ceny Usług uprzednio zakupionych przez Usługobiorców.

**§11 Metody płatności**
1. Usługobiorca, po upływie Okresu testowego i kontynuacji Umowy, zgodnie z par. 13 ust. 1, uiszcza, z góry, w odstępach 30-dniowych miesięczne opłaty z tytułu świadczenia Usług.
2. Dopuszczalne metody płatności, z zastrzeżeniem ust. 4 poniżej, to:
a) karta płatnicza za pośrednictwem serwisu Przelewy24pl.;
b) przelew za pośrednictwem Dotpay;
c) przelew za pośrednictwem Paypal;
d) przelew za pośrednictwem PayU.
3. Pierwsza opłata dokonywana przy zawarciu Umowy, o czym mowa w par. 5 realizowana jest tylko i wyłącznie przy użyciu karty płatniczej. W przypadku gdy Usługodawca dokona co najmniej 7 nieudanych (z powodu braku środków) prób ściągnięcia opłaty z karty płatniczej, uważa się, że Usługobiorca rezygnuje ze świadczenia Usługi i umowa rozwiązuje się.
4. Usługodawca w pierwszej kolejności miesięczną opłatę za świadczenie Usług, ściągnie z karty płatniczej Usługobiorcy (podanej przy procesie zamówienia Usługi). W przypadku nieudanej próby obciążenia karty, do Usługobiorcy wysyłany jest e-mail informujący o tym fakcie, jednocześnie przekazywane są informacje o alternatywnych metodach płatności. W przypadku braku zapłaty, Usługodawca podejmie kolejne próby ściągnięcia środków z karty. Po co najmniej 7 nieudanych (z powodu braku środków) prób ściągnięcia opłaty z karty płatniczej, uważa się, że Usługobiorca rezygnuje ze świadczenia Usługi i Umowa rozwiązuje się.
5. Usługodawca po dokonaniu przez Usługobiorcę opłat opisanych w ust. 1 i 3 wystawia fakturę VAT i przesyła ją na podany przez Usługobiorcę podczas procesu zamówienia Usługi adres email oraz jednocześnie zamieszcza ją w Panelu Usługobiorcy do ew. samodzielnego pobrania przez Usługobiorcę.
6. Jednocześnie Usługobiorca wyraża zgodę na przesyłanie przez Usługodawcę faktury VAT jedynie w formie elektronicznej na adres email, o którym mowa w §4 ust. 4.
7. Faktury VAT wystawiane są przez Usługodawcę nie później niż do 15-ego dnia miesiąca następującego po miesiącu, w którym wykonano Usługę. W przypadku Usługobiorców będących Konsumentami, faktury na zgłoszone żądanie wystawiane są zgodnie z obowiązującymi w tym zakresie przepisami.

**§12 Odstąpienie od umowy o świadczenie Usług**
1. W przypadku gdy Usługobiorca jest Konsumentem ma prawo odstąpić od umowy o świadczenie Usług w terminie 14 dni od zawarcia umowy bez podania przyczyny.
2. Uprawnienie powyższe nie przysługuje, jeśli Usługobiorca będący Konsumentem wyrazi zgodę i zażąda rozpoczęcia świadczenia Usługi przed upływem 14 dni od dnia zawarcia Umowy.
3. Wzór oświadczenia Konsumenta o odstąpieniu od umowy stanowi załącznik nr 1 do Regulaminu.
4. Usługobiorca będący Konsumentem może wysłać oświadczenie wymienione w ust.2 na adres email Usługodawcy wskazany w §14 lub listownie na adres: ul. św. Filipa 23/4, 31-150 Kraków.
5. Uprawnienie powyższe nie przysługuje w przypadku Usługobiorców niebędących Konsumentami.

**§13 Obowiązywanie Umowy**
1. Zawarta w sposób określony w par. 5 umowa o świadczenie Usług obowiązuje przez czas określony – Okres testowy, po którym, w braku odmiennego oświadczenia Usługobiorcy przekształca się w umowę na czas nieokreślony.
2. W Okresie testowym, Usługobiorca może złożyć Usługodawcy oświadczenie o wypowiedzeniu umowy wysyłając mu w tym celu maila na adres: info@funnelstar.io. W takim wypadku, umowa rozwiązuje się z upływem Okresu testowego.
3. Usługobiorca ma prawo wypowiedzieć umowę zawartą na czas nieokreślony z zachowaniem 3-miesięcznego okresu wypowiedzenia ze skutkiem na koniec miesiąca kalendarzowego. Wypowiedzenie winno być dokonane poprzez wysłanie stosownej wiadomości e-mail na adres: info@funnelstar.io. W takim wypadku umowa ulega rozwiązaniu z upływem okresu wypowiedzenia.
4. Usługodawca może wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku, gdy Usługobiorca narusza postanowienia nin. Regulaminu, w szczególności:
a. nie współpracuje z Usługodawcą celem umożliwienia świadczenia Usługi;
b. zalega z płatnościami za Usługę, za co najmniej 2 pełne okresy płatności.

**§14 Kontakt Usługodawcy i Usługobiorcy**
We wszystkich sprawach związanych z realizacją Usługi Usługobiorca może kontaktować się z Usługobiorcą za pośrednictwem adresu e-mail: info@funnelstar.io

**§15 Dane osobowe**
1. Administratorem danych osobowych Usługobiorców przetwarzanych w zw. ze świadczoną Usługą jest Usługodawca.
2. Administrator gwarantuje i zapewnia, że przetwarzanie danych osobowych w ramach zawartej Umowy i świadczonej w zw. z nią Usługą odbywa się zgodnie z zapisami RODO, w szczególności Administrator stosuje odpowiednie środki techniczne i organizacyjne zapewniające bezpieczeństwo przetwarzania.
3. Administrator spełnia obowiązek informacyjny względem Usługobiorców w momencie zbierania od nich danych przy zawieraniu umowy, o czym mowa w par. 5. Jednocześnie odpowiednia nota informacyjna jest umieszczona w Panelu Usługobiorcy, który ma możliwość się z nią zapoznać w każdym czasie.
4. Realizując Usługę na rzecz Usługobiorcy, Usługodawca jako podmiot przetwarzający, przetwarza na zlecenie Usługobiorcy dane osobowe użytkowników jego Bloga, dlatego też wraz z zawarciem umowy o świadczenie Usług, Strony zawierają umowę o powierzeniu przetwarzania danych osobowych, której wzór stanowi zał. nr 2 do nin. Regulaminu. Po zalogowaniu się do Panelu Usługobiorcy, Usługobiorca składa oświadczenie o akceptacji postanowień umownych. Jednocześnie Usługobiorca może pobrać treść tej umowy.
5. Szczegółowe informacje na temat przetwarzania danych osobowych dostępne są też na stronie www.funnelstar.io w zakładce „Polityka prywatności”.

**§16 Postanowienia końcowe**
1. Niniejszy Regulamin obowiązuje od dnia opublikowania go w Serwisie.
2. Usługodawca zastrzega sobie możliwość zmiany Regulaminu, w zakresie dopuszczanym przez obowiązujące przepisy, w szczególności modyfikacje mogą być wynikiem zmian obowiązujących przepisów, przy czym zmiany Regulaminu nie naruszają praw nabytych przez Usługobiorców.
3. Usługodawca za każdym razem poinformuje Usługobiorców o zmianie Regulaminu umieszczając odpowiedni komunikat w Serwisie oraz wysyłając w tym celu wiadomość e-mail. Zmiany Regulaminu wchodzą w życie z chwilą ich publikacji w Serwisie. Jeżeli Usługobiorca nie akceptuje wprowadzonych zmian powinien wypowiedzieć Umowę.
4. Regulamin jest na stałe dostępny nieodpłatnie w Serwisie, na stronie www.funnelstar.io/regulamin w formie, która umożliwia jego pozyskanie, odtwarzanie i utrwalanie za pomocą systemu teleinformatycznego, którym posługuje się Użytkownik.
5. W sprawach nieuregulowanych w niniejszym Regulaminie mają zastosowanie postanowienia Regulaminu Serwisu Funnelstar oraz obowiązujące przepisy prawa polskiego.
6. Wszelkie spory związane z zawartą Umową o świadczenie Usług rozstrzygane będą przez właściwe sądy polskie.
7. W przypadku Usługobiorcy, który jest Konsumentem istnieje możliwość skorzystania z pozasądowego sposobu rozstrzygania sporów. Szczegółowe informacje w tym przedmiocie dostępne są w siedzibach oraz na stronach internetowych powiatowych (miejskich) rzeczników konsumentów, organizacji społecznych, do których zadań statutowych należy ochrona konsumentów (Federacja Konsumentów, Stowarzyszenie Konsumentów Polskich), Wojewódzkich Inspektoratów Inspekcji Handlowej.
