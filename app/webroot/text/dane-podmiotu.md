#INFORMACJE PRAWNE<br/><br/>
**Podmiot**<br/>
TFB Group Spółka z ograniczoną odpowiedzialnością z siedzibą w Krakowie (31-150 Kraków, ul. św. Filipa 23/4) zarejestrowana w rejestrze przedsiębiorców prowadzonym przez Sąd Rejonowy dla Krakowa – Śródmieścia Wydział XI Krajowego Rejestru Sądowego pod nr KRS: 0000499144, NIP 6762474679, REGON 12304891600000

**Kontakt**<br/>
info@funnelstar.io
