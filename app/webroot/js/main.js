(() => {
  const headerContainer = $('.header-container');
  const sidebarContainer = $('.sidebar');
  const close = sidebarContainer.find('.close');
  const hamburger = headerContainer.find('.hamburger');
  const main = $('main');
  const footer = $('footer');
  
  hamburger.click(() => {
    sidebarContainer.addClass('active');
    footer.hide();
  });
  close.click(() => {
    sidebarContainer.removeClass('active');
    footer.show();
  });
})();

(() => {
  const formContainer = $('.form-container');
  const button = formContainer.find('button');
  const name = formContainer.find('.input-name');
  const email = formContainer.find('.input-email');
  const message = formContainer.find('.input-message');

  const errors = {
    name: {
      _empty: 'Imie i nazwisko jest wymagane',
      _required: 'Imie i nazwisko jest wymagane'
    },
    email: {
      _empty: 'E-mail jest wymagany',
      _required: 'E-mail jest wymagany',
      email: 'E-mail nie jest poprawny'
    },
    text: {
      _empty: 'Opis jest wymagany',
      _required: 'Opis jest wymagany'
    }
  };

  const messageAlert = (text, id) => {
    const template = formContainer.prepend(
      `
      <p class="wrong ${id}">
        ${text}
      </pv>
    `
    );
    return template;
  };

  const removeMessageAlert = (id) => {
    const message = formContainer.find(`.wrong.${id}`);
    message.slideUp(300, () => {
      message.remove();
    });
  };

  const addWrongBorder = (inputName) => {
    inputName.addClass('negative');
  };

  const removeWrongBorder = (inputName) => {
    inputName.removeClass('negative');
  };

  const addPositiveValidtion = () => {
    formContainer.hide();
    $('main').append(
      `
      <p class="positive-validation">
        Dziękujemy za wysłanie zgłoszenia
      </pv>
    `
    );
  };

  const messageMethod = () => {
    const messageValue = message.val();
    return new Promise((resolve, reject) => {
      if (messageValue.length === 0 && formContainer.find('.wrong.text').length === 0) {
        messageAlert(errors.text._empty, 'text');
        addWrongBorder(message);
        reject(false);
      }

      if (messageValue.length > 0) {
        removeWrongBorder(message);
        removeMessageAlert('text')
        resolve(true);
      }
    });
  };

  const nameMethod = () => {
    const nameValue = name.val();
    return new Promise((resolve, rejece) => {
      if (nameValue.length === 0 && formContainer.find('.wrong.name').length === 0) {
        messageAlert(errors.name._empty, 'name');
        addWrongBorder(name);
        reject(false);
      }

      if (nameValue.length > 0) {
        removeWrongBorder(name);
        removeMessageAlert('name')
        resolve(true);
      }
    });
  };

  const emailMethod = () => {
    const emailValue = email.val();
    const pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    const isEmail = pattern.test(emailValue);

    return new Promise((resolve, reject) => {
      if (formContainer.find('.wrong.email').text().trim() === errors.email._empty && !(isEmail) && emailValue.length > 0) {
        removeMessageAlert('email');
        messageAlert(errors.email.email, 'email');
        resolve(true);
      }

      if (formContainer.find('.wrong.email').text().trim() === errors.email.email && emailValue.length === 0) {
        removeMessageAlert('email');
        messageAlert(errors.email._empty, 'email');
        resolve(true);
      }

      if (!(isEmail) && emailValue.length > 0 && formContainer.find('.wrong.email').length === 0) {
        messageAlert(errors.email.email, 'email');
        addWrongBorder(email);
        reject(false);
        console.log('asas')
      }

      if (emailValue.length === 0 && formContainer.find('.wrong.email').length === 0) {
        messageAlert(errors.email._empty, 'email');
        addWrongBorder(email);
        reject(false);
      }

      if (isEmail) {
        removeMessageAlert('email');
        removeWrongBorder(email);
        resolve(true);
      }
    });
  };

  message.blur(() => {
    messageMethod().catch((error) => error);
  });
  name.blur(() => {
    nameMethod().catch((error) => error);
  });
  email.blur(() => {
    emailMethod().catch((error) => error);
  });

  button.click(() => {
    const url = formContainer.attr('action');
    h1TextValue = $('.header-container').find('h1').text().trim();
    Promise.all([nameMethod(), emailMethod(), messageMethod()]).then((values) => {
      button.attr('disabled', 'disabled');
      $.ajax({
        url: url,
        type: 'post',
        beforeSend: (xhr) => {
          xhr.setRequestHeader('X-CSRF-Token', formContainer.find('[name="_csrfToken"]').val());
        },
        data: {
          email: email.val(),
          name: name.val(),
          text: message.val(),
          h1: h1TextValue
        }
      })
      .done((response) => {
        if (response.status == 'succes') {
          addPositiveValidtion();
        } else {
          if (response.errors) {
            for (let key in response.errors) {
              if (Object.keys(response.errors[key]).length > 0) {
                const keyOne = Object.keys(response.errors[key])[0];
                const messageNotExist = formContainer.find('.wrong').hasClass(key);
                if (errors[key] && errors[key][keyOne] && !messageNotExist) {
                  messageAlert(errors[key][keyOne], key);
                } else {
                  messageAlert(response.errors[key][keyOne], key);
                }
              } else {
                if (formContainer.find('p').hasClass('undefined')) {
                } else {
                  messageAlert('Przepraszamy, błąd serwera');
                }
              }
            }
          } else if(response.message) {
            messageAlert(response.message);
          } else {
            if (formContainer.find('p').hasClass('undefined')) {
            } else {
              messageAlert('Przepraszamy, błąd serwera');
            }
          }
        }
      })
      .fail(() => {
        if (formContainer.find('p').hasClass('undefined')) {
        } else {
          messageAlert('Przepraszamy, błąd serwera');
        }
      })
      .always(() => {
        button.removeAttr('disabled', 'diabled');
      });
    })
    .catch((error) => {
    });
  })
})();

(() => {
  const closeCookies = document.querySelector('.close-cookies');
  const cookiesInformation = document.querySelector('.cookies-information');

  if (Cookies.get('privacyPolicy')) {
    cookiesInformation.classList.add('close');
  }

  if (!(Cookies.get('privacyPolicy'))) {
    cookiesInformation.classList.add('show');
  }

  closeCookies.addEventListener('click', () => {
    cookiesInformation.classList.remove('show');
    cookiesInformation.classList.add('close');
    Cookies.set('privacyPolicy', 1, { expires: 30 });
  });
})();