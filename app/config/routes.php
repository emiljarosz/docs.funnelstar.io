<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->redirect('/', ['controller' => 'Regulations', 'action' => 'main', 'regulamin-swiadczenia-uslug']);
    //$routes->redirect('/*', ['controller' => 'Regulations', 'action' => 'main', 'regulamin-swiadczenia-uslug']);
    $routes->connect('/:slug', ['controller' => 'Regulations', 'action' => 'main'], ['pass' => ['slug']]);
    $routes->fallbacks(DashedRoute::class);
});
