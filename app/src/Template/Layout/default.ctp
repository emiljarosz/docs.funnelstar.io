<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,500,600,700&amp;subset=latin-ext" rel="stylesheet">
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    $title = $this->fetch('title');

    echo '<title>', $title, '</title>';
    echo $this->fetch('meta');
    echo $this->fetch('css');

    $httpHost = env('HTTP_HOST');
    $isDev = (strrpos($httpHost, '.test') !== false);

    if ($isDev):
        echo $this->Html->css('/css/style');
    else:
        echo $this->Html->css('/css/style.min');
    endif;
    ?>
</head>
<body>
    <?php
    echo $this->fetch('content'); 
    echo $this->element('cookies');
    ?>
    
    <?php
    if ($isDev):
        echo $this->Html->script('/js/libs');
        echo $this->Html->script('/js/main');
    else:
        echo $this->Html->script('/js/scripts.min');
    endif;
    ?>
</body>
</html>
