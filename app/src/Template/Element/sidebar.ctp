<?php
use Cake\Routing\Router;
?>

<div class="sidebar">
    <img src="/img/close.svg" alt="Close menu" class="close">
    <h1 class="docs">Dokumenty</h1>
    <ul>
        <?php
        foreach ($results as $result):
        ?>
        <li class="<?php
            $slug = $this->request->getParam('slug');
            if ($slug == $result->slug) {
                echo "active";
            }
        ?>">
            <a href="<?php echo Router::url(['controller' => 'Regulations', 'action' => 'main', $result->slug]); ?>"><?php echo $result->title; ?></a>
        <?php
        echo "</li>";
        endforeach;
        ?>
    </ul>
    <h1 class="forms">Formularze</h1>
    <ul>
        <?php
        foreach ($resultsForm as $result):
        ?>  
        <li class="<?php
            $slug = $this->request->getParam('slug');
            if ($slug == $result->slug) {
                echo "active";
            }
        ?>">
            <a href="<?php echo Router::url(['controller' => 'Regulations', 'action' => 'main', $result->slug]); ?>"><?php echo $result->title; ?></a>
        <?php
        echo "</li>";
        endforeach;
        ?>
    </ul>
</div>