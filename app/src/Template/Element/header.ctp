<header>
    <div class="header-container">
        <img src="/img/hamburger.svg" alt="Hamburger menu" class="hamburger">
        <div class="header-logo">
            <a href="#">
                <img src="img/logo.png" alt="logo">
            </a>
            <a href="#" class="docs">
                DOCS
            </a>
        </div>
    </div>
</header>