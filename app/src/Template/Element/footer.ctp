<?php
use Cake\Routing\Router;
?>
<footer>
    <div class="container">
        <a href="">FunnelStar.io</a>
        <p> © </p>
        <a href="<?php echo Router::url(['controller' => 'Regulations', 'action' => 'main', 'regulamin-swiadczenia-uslug']); ?>">TOS</a>
        <p>, </p>
        <a href="<?php echo Router::url(['controller' => 'Regulations', 'action' => 'main', 'dane-podmiotu']); ?>">Legal</a>
        <p>, </p>
        <a href="<?php echo Router::url(['controller' => 'Regulations', 'action' => 'main', 'polityka-prywatnosci']); ?>">Privacy policy</a>
    </div>
</footer>