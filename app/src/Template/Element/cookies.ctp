<div class="cookies-information">
    <p>Strona korzysta z <a href="#">plików cookies</a> w celu realizacji usług. Jeżeli nie zmienisz ustawień, będą one zapisywanie w pamięci Twojego urządzenia.</p>
    <div class="close-cookies">
        <span class="iconfont iconfont-close"></span>
    </div>
</div>