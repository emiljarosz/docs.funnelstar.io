<div class="header-container">
    <h1>
    FORMULARZ REKLAMACYJNY
    </br>
    </br>
    </h1>
</div>
<div class="company-info">
    <ul>
        <li>FunnelStar (dawniej GetAll.pl)</li>
        <li>należy do:</li>
        <li>TFB GROUP Sp.z.o.o</li>
        <li>Św. Filipa 23/4</li>
        <li>31-150 Kraków</li>
        <li>NIP: 6762474679</li>
        <li>KRS: 0000499144</li>
    </ul>
</div>
<div class="text-container">
<p>lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit ametlorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet</p>
</div>
<?php
    echo $this->Form->create(null, [
        'url' => ['controller' => 'Ajax', 'action' => 'ReclamationForm'],
        'class' => 'form-container'
    ]);
?>
    <input type="text" class="input-name input-group-text" placeholder="Podaj imię i nazwisko...">
    <input type="text" class="input-email input-group-text" placeholder="Podaj adres email...">
    <textarea class="input-message form-control" placeholder="Wpisz wiadomość..."></textarea>
    <button type="button" class="btn">Wyślij</button>
</div>
