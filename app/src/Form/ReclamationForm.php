<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Validation\Validator;
 
class ReclamationForm extends Form {

    public function _buildValidator(Validator $validator) {
        $validator->requirePresence('email', true, 'E-mail jest wymagany');
        $validator->notEmpty('email', 'E-mail jest wymagany');
        $validator->email('email', true, 'E-mail nie jest poprawny');

        $validator->requirePresence('text', true, 'Opis jest wymagany');
        $validator->notEmpty('text', 'Opis jest wymagany');

        $validator->requirePresence('name', true, 'Imie i nazwisko jest wymagane');
        $validator->notEmpty('name', 'Imie i nazwisko jest wymagane');

        return $validator;
    }

    public function _execute(array $data) {
        return true;
    }
}
?>