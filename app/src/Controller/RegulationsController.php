<?php
namespace App\Controller;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

class RegulationsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Markdown.Markdown');
    }

    public function main() {
        $fileMenu = new File(WWW_ROOT.'text/dokumenty.txt');
        $fileMenuRead = $fileMenu->read();
        $results = json_decode($fileMenuRead);
        $this->set('results', $results);
        $slug = $this->request->getParam('slug');
        foreach ($results as $result) {
            if ($result->slug == $slug) {
                if (!empty($result->file)) {
                    $md = file_get_contents(WWW_ROOT.'text/'.$result->file, true);
                    $html = $this->Markdown->parse($md);
                    $this->set('docsContent', $html);
                    $this->set(compact('html'));
                }
            }
        }

        $formMenu = new File(WWW_ROOT.'text/formularze.txt');
        $formMenuRead = $formMenu->read();
        $resultsForm = json_decode($formMenuRead);
        $this->set('resultsForm', $resultsForm);
        foreach ($resultsForm as $result) {
            if ($result->slug == $slug && $result->type == "text") {
                if (!empty($result->file)) {
                    $md = file_get_contents(WWW_ROOT.'text/'.$result->file, true);
                    $html = $this->Markdown->parse($md);
                    $this->set('formContent', $html);
                    $this->set(compact('html'));
                }
            } 
            if ($result->slug == $slug && $result->type == "ctp") {
                $slug1 = $this->request->getParam('slug');
                $ctpFile = $result->file;
                $this->set('ctpFile', $ctpFile);
            }
        }
    }
}
?>