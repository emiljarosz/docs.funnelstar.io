<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Form\Form;
use Cake\Form\Schema;
use App\Form\ReclamationForm;
use Cake\Validation\Validator;
use Zendesk\API\HttpClient as ZendeskAPI;
use Cake\Http\Client;
use Cake\I18n\Time;

class AjaxController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->autoRender = false;
    }

    protected function fakeMethodConnectionApi(&$responseData) {
        $requestData = $this->request->getData();

        if (!empty($requestData['email']) && $requestData['email'] === 'fake@getall.pl') {
            $responseData['status'] = 'error';
            $responseData['message'] = 'Błąd Api';
        }
    }

    public function ReclamationForm() {
        $data = ['status' => 'succes'];
        $h1 = $this->request->data['h1'];

        if( $this->request->is('ajax') && $this->request->isPost()) {
            $form = new ReclamationForm();
            $isValid = $form->validate($this->request->getData());

            if (!$isValid) {
                $errors = $form->errors();
                $data['status'] = 'error';
                if (!empty($errors)) {
                    $data['errors'] = $errors;
                }
            }
            $this->fakeMethodConnectionApi($data);

        if ($data = ['status' => 'succes']) {
            $zendeskDataEmail = $this->request->data['email'];
            $zendeskDataName = $this->request->data['name'];
            $zendeskDataMessage = $this->request->data['text'];
            $timeNowClass = Time::now();
            $timeNow = "[{$timeNowClass->year}/{$timeNowClass->month}/{$timeNowClass->day}]";

            if ($h1 == "FORMULARZ REKLAMACYJNY") {
                $zendeskArrayToSend = [
                    'subject'  => "Reklamacja - {$timeNow}",
                    'comment'  => array(
                        'body' => $zendeskDataMessage
                    ),
                    'requester' => array(
                        'name' => $zendeskDataName,
                        'email' => $zendeskDataEmail,
                    )
                ];
            } else {
                $zendeskArrayToSend = [
                    'subject'  => "Pytanie ze strony",
                    'comment'  => array(
                        'body' => $zendeskDataMessage
                    ),
                    'requester' => array(
                        'name' => $zendeskDataName,
                        'email' => $zendeskDataEmail,
                    )
                ];
            }

            $subdomain = "getall";
            $username  = "support@getall.pl";
            $token     = "Tj6k1M8HbORchj6uowturAq1sHaTS1xGB5oHO3C7";
            $client = new ZendeskAPI($subdomain);
            $client->setAuth('basic', ['username' => $username, 'token' => $token]);
            $newTicket = $client->tickets()->create($zendeskArrayToSend);
        }

        return $this->response
            ->withType('json')
            ->withStringBody(json_encode($data));
        }
    }
    
}
