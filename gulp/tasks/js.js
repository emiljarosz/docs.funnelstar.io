const gulp = require('gulp');
const util = require('gulp-util');
const plumber = require('gulp-plumber');
const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const size = require('gulp-size');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');

const config = require('../config');

const list = [
  'bower_components/promise-polyfill/dist/polyfill.js',
  'bower_components/jquery/dist/jquery.js',
  'bower_components/js-cookie/src/js.cookie.js',
];

const src = ['./app/webroot/js/main.js'];
const dest = './app/webroot/js';

const babelConf = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: '> 0.25%, not dead',
        debug: true
      }
    ]
  ]
};

gulp.task('js', () => {
  return gulp.src(config.production ? list.concat(src) : list)
    .pipe(plumber())
    .pipe(config.production ? util.noop() : sourcemaps.init())
    .pipe(config.production ? babel(babelConf) : util.noop())
    .pipe(config.production ? uglify() : util.noop())
    .pipe(concat(config.production ? 'scripts.js' : 'libs.js'))
    .pipe(config.production ? util.noop() : sourcemaps.write('.'))
    .pipe(config.production ? rename({ suffix: '.min' }) : util.noop())
    .pipe(size())
    .pipe(gulp.dest(dest))
    .on('error', function(error) {
      console.log(error);
    });
});