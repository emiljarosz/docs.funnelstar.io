const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const util = require('gulp-util');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');

const config = require('../config');

const src = './scss/style.scss';
const dest = './app/webroot/css';

gulp.task('sass', () => {
	return gulp.src(src)
	.pipe(config.production ? util.noop() : sourcemaps.init())
	.pipe(config.production ? sass({ outputStyle: 'compressed' }) : util.noop())
	.pipe(config.production ? util.noop() : sass({ outputStyle: 'expanded' }))
	.pipe(autoprefixer({
      browsers: ['last 11 versions'],
      cascade: false
	}))
	.pipe(config.production ? util.noop() : sourcemaps.write('.'))
	.pipe(config.production ? rename({ suffix: '.min' }) : util.noop())
	.pipe(gulp.dest(dest))
});