const util = require('gulp-util');

module.exports = {
  // Jeśli przekazano parametr "--production" to production = true
  production: !!util.env.production
};